import yaml

from ifs.iteratedfunctionsystem import IteratedFunctionSystem as IFS
from ifs.userinterface import UserInterface


def plot(name='fern'):
    with open('ifs/parameters.yaml', 'r') as file:
        parameters = yaml.load(file, Loader=yaml.FullLoader)
    functions_params = parameters[name]
    ifs = IFS(functions_params)
    ifs.run(n_iterations=50000)
    ifs.plot()


def run(name):
    UserInterface(name=name).run()


if __name__ == '__main__':
    name = 'fern'    
    run(name)