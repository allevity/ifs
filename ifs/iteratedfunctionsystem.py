import numpy as np
import matplotlib.pyplot as plt
import random


class IteratedFunctionSystem(object):
    def __init__(self, function_params, ifs_name='', n_iterations=None, weights=None):
        self.ifs_name = ifs_name
        self.functions = [self._param2function(param) for param in function_params]
        self.points = None
        self.n_point_plot = 0
        self.weights = None if weights is None else [k/sum(weights) for k in weights]  # renormalise weights to 1
        if n_iterations:
            self.run(n_iterations=n_iterations)

    def run(self, init_point=(0, 0), n_iterations=10000):
        points = np.zeros([n_iterations, 2])  # init array
        points[0, :] = np.array(init_point)
        for n in range(1, n_iterations):
            f = self._random_fn()
            points[n, :] = f(points[n-1, :])
        self.points = points
        self.n_point_plot = n_iterations

    @classmethod
    def _param2function(cls, p):
        return lambda x: (p[0] * x[0] + p[1] * x[1] + p[4], p[2] * x[0] + p[3] * x[1] + p[5])

    def _random_fn(self):
        return np.random.choice(self.functions, p=self.weights)

    def plot(self, n=None):
        n = n or self.n_point_plot
        plt.plot(self.points[:n, 0], self.points[:n, 1], 'b.', 'markersize', 1)
        plt.show()
