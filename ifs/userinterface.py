from appJar import gui
import yaml
from ifs.iteratedfunctionsystem import IteratedFunctionSystem as IFS
import matplotlib.colors as mcolors


class UserInterface(object):
    PARAMETERS_FILE = 'ifs/parameters.yaml'
    title_n_points_widget = 'Number of points'
    with open(PARAMETERS_FILE, 'r') as file:
        IFS_PARAMS = yaml.safe_load(file)
    LIST_IFS = IFS_PARAMS.keys()
    CALCULATED_IFS = {}  # {ifs_name: ifs}

    def __init__(self, name='fern', n_points_plot=0, max_n_points=100000):
        self.app = gui()  # create a GUI variable called app
        self.axes = None
        self.max_n_points = max_n_points
        self.n_points_plot = n_points_plot
        self._init_ifs(name, refresh=False)

    def _init_ifs(self, ifs_name, refresh=True):
        ifs_params = self.IFS_PARAMS[ifs_name]
        try:
            self.ifs = self.CALCULATED_IFS[ifs_name]  # Just fetched saved data
        except KeyError:
            self.ifs = IFS(ifs_params['params'], weights=ifs_params.get('weights', None), ifs_name=ifs_name, n_iterations=self.max_n_points)
        self.points = self.ifs.points
        x_lim = ifs_params['xlim']
        y_lim = ifs_params['ylim']
        self.plot_options = {'color': 'darkviolet', 'marker': '.', 'linestyle': "", 'markersize': 0.2}
        self.axes_options = {'set_xlim': x_lim, 'set_ylim': y_lim}
        if refresh:
            self.refresh_plot()

    def run(self):
        # add & configure widgets - widgets get a name, to help referencing them later
        app = self.app
        app.setBg("OldLace")  # http://appjar.info/pythonBasics/#colour-map
        app.addLabelOptionBox("IFS", self.LIST_IFS, callfunction=True)
        app.setLabel("IFS", "I.F.S.")
        app.setOptionBox("IFS", self.ifs.ifs_name)
        app.setOptionBoxChangeFunction("IFS", lambda x: self.change_plot())

        app.setSize(600, 800)
        app.addLabel("title", "IFS")
        app.setLabelBg("title", "orange")
        # app.addMessage('InputMessage','Input wav path or Youtube URL:')
        # app.setMessageWidth('InputMessage', 500)

        # app.addButtons(["Number of points", "Close"], self._press)
        # self.axes = app.addPlot("Plot", *getXY(), showNav=False)
        # self.app.slider('yVal', show=True, pos=[2, 0])
        # self.app.slider(self.title_n_points_widget, show=True, nav=False)
        n = self.n_points_plot
        X, Y = self.ifs.points[:n, 0], self.ifs.points[:n, 1]

        # self.axes.set_prop_cycle(color=['red'], marker=['.'])
        # self.axes = self.app.addPlot("Plot", *getXY(), row=1)
        # axes = self.axes
        # # axes.legend(["key data"])
        # axes.set_xlabel("")
        # axes.set_ylabel("")
        # # axes.set_title("title")
        app.addLabel("l1", "empty", 10, 10)
        # app.addLabel("l2", "empty", 0, 10)
        # app.addLabel("l3", "empty", 0, 10)
        by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgb(color))),
                         name)
                        for name, color in mcolors.CSS4_COLORS.items())
        names = [name for hsv, name in by_hsv]
        app.addLabelOptionBox("Color", names, callfunction=True)
        app.setOptionBoxChangeFunction("Color", lambda x: self.refresh_plot())
        self.plot_options['color'] = app.getOptionBox('Color')
        self.axes = self.app.plot("Plot", X, Y, row=1, plot_options=self.plot_options)
        self.axes.set_xlim(self.axes_options['set_xlim'])
        self.axes.set_ylim(self.axes_options['set_ylim'])

        self._add_parameter(self.title_n_points_widget, _from=1, _to=self.max_n_points)
        self.app.setScaleChangeFunction(self.title_n_points_widget, self.refresh_plot)
        app.go()

    def change_plot(self):
        ifs_name = self.app.getOptionBox('IFS')
        if ifs_name == self.ifs.ifs_name:
            return
        self._init_ifs(ifs_name, refresh=True)

    def _add_parameter(self, title, _from=1, _to=None):
        self.app.addLabelScale(title)
        self.app.showScaleValue(title, show=True)
        self.app.setScaleRange(title, _from, _to)

    def refresh_plot(self):
        n = self.n_points_plot = self.app.getScale(self.title_n_points_widget)
        X, Y = self.ifs.points[:n, 0], self.ifs.points[:n, 1]
        self.plot_options['color'] = self.app.getOptionBox('Color')
        self.axes = self.app.updatePlot("Plot", X, Y, keepLabels=False, plot_options=self.plot_options, axes_options=self.axes_options)
        # self.axes.set_xlim([-2, 2])
        # self.axes.set_ylim([-0.5, 8.5])


if __name__ == '__main__':
    ui = UserInterface()
    ui.run()
